import React, { Component } from 'react';
import './App.css';
import {About} from './components/About';
import {CrisPam} from './components/CrisPam';
import {CrisPamDB} from './components/CrisPamDB';
import {Contact} from './components/Contact';
import {Home} from './components/Home';
import {References} from './components/References';

class App extends Component {
  constructor (props){
    super(props);
    this.menuButtonClickHander = this.menuButtonClickHander.bind(this);
    this.state = {
      pageName : "CrisPam",
    }
  }

  menuButtonClickHander (buttonPressed){
    this.setState({
      pageName: buttonPressed,
    })
  }

  render() {
    // Build menu
    let menuItems = [
      {name: 'Home'},
      {name: 'CrisPam'},
      {name: 'CrisPamDB'},
      {name: 'About'},
      {name: 'References'},
      {name: 'Contact'},
   ]
   let menuDiv = 
   <div> 
    {menuItems.map((menuItem, i)=> (
        <ul className = 'Menu-Title' key={i}>
            <li><div onClick={(i)=> this.menuButtonClickHander(menuItem.name)}>{menuItem.name}</div></li>
        </ul>
      ))
    }
  </div>

  // build component
  let pageComponent;
    switch (this.state.pageName) {
      case "Home":
        pageComponent = <Home></Home>;
        break;
      case "CrisPam":
        pageComponent = <CrisPam></CrisPam>;
        break;
      case "CrisPamDB":
        pageComponent = <CrisPamDB></CrisPamDB>;
        break;
      case "About":
        pageComponent = <About></About>;
        break;
      case "Contact":
       pageComponent = <Contact></Contact>;
        break;
      case "References":
        pageComponent = <References></References>;
        break;
      default:
        pageComponent = <Home></Home>;
        break;
    }
    return (
      <div className = "App">
        <div className = "App-Menu">
          {menuDiv} {/*menu */}
        </div>
        <div className = "content">
          {pageComponent}
        </div>
        <div className = "footer">
          <footer>
            <b>Offen's Lab for Translational Neuroscience</b>

            
          </footer>
        </div>
      </div>
    );
  }
}

export default App;