import React, { Component } from 'react';
import '../Fonts.css';
import '../App.css';

class References extends Component {
    constructor (props){
      super(props);
      this.state = {
      }
    }
    render() {
        return (
        <div>
            <h1 className='Header'> {/*header */}
                Copyrights and Citing:
            </h1> 
            <p className= 'BlockText'>
                If you use CrisPam web tool, CrisPam databases or any part of it, please cite:<br></br>
                Rabinowitz R, Darnell R, Offen D. (2019) <br></br>
                CrisPam: SNP-derived PAM analysis web tool and human pathogenic SNPs database for CRISPR allele-specific targeting
            </p>
        </div>
        );
    }
}
export {References};