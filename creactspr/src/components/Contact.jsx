import React, { Component } from 'react';
import '../Fonts.css';
import '../App.css';


class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

      render() {
        return (
          <div>
            <h1 className='Header'> {/*header */}
              Contact
            </h1> 
            <p className= 'BlockText'>
              For technical support and suggestions please contact Roy Rabinowitz – Rabinowitz.roy@gmail.com<br></br>
              CrisPam is licensed under the Academic Free License<br></br>
              For commercial purposes please contact Offen’s Lab<br></br>
              <a href="https://www.danioffenlab.com/">Dani offen's site</a>
            </p>
          </div>
        );
    }
}

export {Contact};