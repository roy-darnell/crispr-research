import React, { Component } from 'react';
import '../Fonts.css';
import ReactTable from 'react-table'
import "react-table/react-table.css";

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function calcComp(sequence){
    //replace A->B->T,T->U->A,G->H->C,C->D->G
    sequence = reverseString(sequence)
    sequence = replaceAll(sequence,"A","B");
    sequence = replaceAll(sequence,"T","U");
    sequence = replaceAll(sequence,"G","H");
    sequence = replaceAll(sequence,"C","D");
    sequence = replaceAll(sequence,"B","T");
    sequence = replaceAll(sequence,"U","A");
    sequence = replaceAll(sequence,"H","C");
    sequence = replaceAll(sequence,"D","G");
    sequence = replaceAll(sequence,"<","L");
    sequence = replaceAll(sequence,">","R");
    sequence = replaceAll(sequence,"L",">");
    sequence = replaceAll(sequence,"R","<");
    return sequence;
}

function reverseString(str) {
    var splitString = str.split("");
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join("");
    return joinArray;
}


class ResultsTable extends Component {
    constructor (props){
      super(props);
      this.state = {
      }
    }

    render() {
        console.log("Results() component rendered");
        let data = this.props.data; //String: "<name of cas> on index: <# of index>"
        // console.log("debug: ",data);
        let sequence = this.props.sequence; //String, Variant sequence 
        // console.log("debug: seq: ",sequence);
        let test_data = [];
        let marked_sequence = "";
        let ERROR_STRING = undefined;
        if (data !== undefined){
            data = data.split(".\n"); //split to different cases and indexes
            if (data[0] !== undefined){ //Split worked
                if (!isNaN(parseInt(data[0].slice(-1)))) { //Analysis not error 
                    data.splice(-1,1) //remove last element from list
                    data.forEach(function(item) {
                        let comp = false;
                        marked_sequence = "";
                        let temp_sequence = sequence;
                        if (item.search("comp.") !== -1){item = item.replace("comp. ",""); comp= true;} //identify complementary case
                        let cas_length = item.search(":");
                        let cas_name = item.substring(0,cas_length);
                        let index_loc = item.search("index: ") + 6;
                        let index = item.substring(index_loc);
                        index = parseInt(index);
                        let PAM_start = item.search(": ")+2;
                        let PAM_end = item.search(" on");
                        let PAM_seq = item.substring(PAM_start,PAM_end);
                        let PAM_length = PAM_end-PAM_start;
                        marked_sequence += temp_sequence.substring(index-5,index)+"<"+temp_sequence.substring(index,index+PAM_length)+">"+temp_sequence.substring(index+PAM_length,index+PAM_length+1+5);
                        if (comp){marked_sequence = calcComp(marked_sequence); marked_sequence = "On Comp. "+marked_sequence;} //if comp, reverse the sequence and replace letters
                        test_data.push({CAS: cas_name,PAM: PAM_seq,Sequence: marked_sequence})
                    });
                }
            }
            if (data[0].includes("Error")) { //print error
                ERROR_STRING = data;
            }
        }
   
        const columns = [{
            Header: 'CAS',
            accessor: 'CAS'
           },{
            Header: 'PAM',
            accessor: 'PAM'
           },{
            Header: 'Sequence',
            accessor: 'Sequence'
           }
        ]
        return (
            <div>
                <p>{ERROR_STRING}</p>
              <ReactTable
                data={test_data}
                columns={columns}
                defaultPageSize = {5}
                pageSizeOptions = {[5, 10, 20]}
              />
            </div>
        );
    }
}

export {ResultsTable};