import React, { Component } from 'react';
import '../Fonts.css';
import '../App.css';


class CrisPamDB extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

      render() {
        return (
          <div>
            <h1 className='Header'> {/*header */}
              CrisPamDB
            </h1> 
            <p className= 'BlockText'>
              CrisPam db contains all reported pathogenic and likely-pathogenic SNPs in humans that were found to be “PAM-generators”. <br></br>
              A SNP is a PAM-generator if the base replacement generates a PAM sequence, the recognition and binding site of the Cas protein. <br></br>
              Therefore, a matching SNP is such that a PAM sequence exists at the variant sequence but not at the reference sequence.<br></br>
              <br></br>
              The CrisPam db contains 53,599 matching SNPs and their corresponding Cas proteins for SNP-derived PAM targeting.<br></br>
              <br></br>
              In case a SNP of interest does not appear on the CrisPam db, it is recommended to try and find it at our complete database of scanned SNPs. <br></br>
              The complete database contains 64,581 SNPs in total (49,634 pathogenic and 14,947 likely-pathogenic SNPs). <br></br>
              If it is found at the complete database but not at the CrisPam database, it may not be a PAM-generator SNP (at least for the 16 PAM sequences CrisPam scans by default).<br></br>
              Otherwise, if a SNP does not appear in our complete database of SNPs, it either may not be or it is not reported as a pathogenic SNP in the original dataset from NCBI’s SNP database. <br></br>
              In that case, the SNP can be analyzed by entering its reference and variation DNA sequences or by entering SNP ID (for SNPs in NCBI’s SNP).<br></br>
              <br></br>
              <b>Downloads:</b><br></br>
              <br></br>
              <b><a href="http://crispr.tau.ac.il/DBs/CrisPam_results.xlsx">CrisPam db</a><br></br></b>
              <b><a href="http://crispr.tau.ac.il/DBs/AnalysedSNPs.xlsx">Complete database of SNP's scanned by CrisPam</a></b>
            </p>
          </div>
        );
    }
}

export {CrisPamDB};