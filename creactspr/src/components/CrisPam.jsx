import React, { Component } from 'react';
import "./CrisPam.css";
import '../Fonts.css';
import {ResultsTable} from "./Results.jsx"

class CrisPam extends Component {
    constructor (props){
      super(props);
      console.log("CrisPam component constructed");
      //States
      this.state = {
          WTString: "GGATTATCAAGGTCCCTTGGGGAATAAGGACCTCAGTGTGG",
          VarString: "GGATTATCAAGGTCCCTTGGAGAATAAGGACCTCAGTGTGG",
          FileString: "File location",
          resultData : undefined //{ANSWER:"EQR SpCas9: NGAG on index: 18.\nVQR SpCas9: NGAN on index: 18.\nStCas9: NNAGAAW on index: 18.\nxCas9: HGA on index: 20.\nAsCas12a RR: TYCV on index: comp. 20.\n"} //{ANSWER: "Error: oh no!"}
      }
      //Fields
      this.result = undefined;

      //Bindings
      this.PostString = this.PostString.bind(this);
      this.PostFile = this.PostFile.bind(this);
      this.VarOnChange = this.VarOnChange.bind(this);
      this.WTOnChange = this.WTOnChange.bind(this);
    }

    PostString(event){
        if (this.state.WTString !== undefined && this.state.VarString !== undefined) { //check strings exist
            var url = '/sequence_submition';
            var data = {"WT": this.state.WTString, "Variant": this.state.VarString};
            console.log("Post Strings:",data);
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {'Accept': 'application/json','Content-Type': 'application/json'},
                })
                .then(response => response.json())
                .then(data => this.setState({resultData:JSON.parse(data)}))
                .catch(error => console.error('Error:', error));
                console.log("data recieved: ",this.state.resultData);
        }
        else {
            alert("Please fill both strings before submition");
        }
    }

    PostFile(event){
        console.log("PostFile() called");
        let formData = new FormData();
        let fileField = document.querySelector("input[type='file']");
        if (fileField !== null && fileField !== undefined){
            formData.append(fileField.files[0]);
            fetch('/file_submition', {
            method: 'PUT',
            body: formData
            })
            .then(response => this.setState({resultData: JSON.parse(response)}))
            .catch(error => console.error('Error:', error));
        } else {
            console.log("No file selected to send");
        }
        
    }

    WTOnChange(event){
        this.setState({WTString: event.target.value});
    }

    VarOnChange(event){
        this.setState({VarString: event.target.value});
    }

    shouldComponentUpdate(nextProps, nextState){
        // console.log("debug",nextState.resultData);
        if (this.state.resultData !== undefined && nextState.userEvents !== undefined){ 
            if(!(this.state.resultData).equals(nextState.userEvents)){
                return true
            }
        }
        return true
    }


    render() {
        console.log("CrisPam component rendered");
        let result = undefined;
        if (this.state.resultData !== undefined ){
            result = 
            <div>
                <br></br>
                <b>Result:</b>
                <br></br>
                <ResultsTable data={this.state.resultData['ANSWER']} sequence={this.state.VarString}></ResultsTable>
                <br></br>
            </div>
        }


        return (
            <div> 
                <h1 className='Header'> {/*header */}
                    CrisPam
                </h1> 
                <p className= 'BlockText'>
                    <b>Instructions: </b><br></br>
                    - sequence length: 40 to 60 bp. <br></br>
                    - maximum variation between sequences: 1.<br></br>
                    - at least 20 bp before and after variation. <br></br>
                    - Legal letters: a,g,t,c,A,G,T,C and '-' for an indel.<br></br>
                    - variation must be shorter than the Reference Allele sequence.<br></br>
                </p>
                <form>
                    <b>Enter reference Sequence here:</b><br></br>
                    <input type="text" name="WTsequence" onChange={this.WTOnChange} value={this.state.WTString} /><br></br>
                    <b>Enter variant Sequence here:</b><br></br>
                    <input type="text" name="VARsequence" onChange={this.VarOnChange} value={this.state.VarString}/>
                    <br></br>
                </form>
                <button className = "Analyze-button" onClick = {this.PostString}>Analyze Sequences</button>
                <br></br>
                {result}
            </div>
        );
    }
}
export {CrisPam};