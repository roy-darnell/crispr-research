import React, { Component } from 'react';
import '../Fonts.css';

class Home extends Component {
    constructor (props){
      super(props);
      this.state = {
      }
    }
    render() {
        return (
            <div>
                <h1 className='Header'> {/*header */}
                    CrisPam tool
                </h1> 
                <p className= 'BlockText'>
                CrisPam is a tool for designing gRNA sequences to specifically target a variant allele using the CRISPR system.<br></br>
                Utilizing a SNP-derived PAM targeting approach increases the specificity of genome editing to the target allele; <br></br>
                thus, it may be useful for studying and treating genetic conditions caused by autosomal dominant mutations.<br></br>
                <br></br>
                The CrisPam tool can be simply used by entering the reference (wildtype) and the variant sequences. <br></br>
                Soon more features will be enabled (analyzing SNPs by SNP ID, analyzing multiple SNPs, customized PAM sequences and more)<br></br>

                CrisPam DB is a database of all known human pathogenic and likely-pathogenic SNPs that were analyzed using CrisPam; <br></br>
                to detect suitable target SNPs for SNP-derived PAM targeting approach using 14 different Cas proteins. <br></br>
                The original dataset of SNPs obtained from NCBI’s SNP database. <br></br>

                For more information please read our manuscript:<br></br>
                Rabinowitz R., Darnell R. and Offen D. (2019)<br></br>
                <br></br>
                <img src="https://i.ibb.co/hFPPtBq/Analysis-example.png" alt="Analysis-example" border="0"></img>
                </p>
            </div>
        );
    }
}
export {Home};